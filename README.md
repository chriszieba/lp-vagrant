LogicPull Vagrant Installation
----

Move into the  directory where the `Vagrantfile` is and run `vagrant up`.

If you see the error

```
The chef binary (either `chef-solo` or `chef-client`) was not found on
the VM and is required for chef provisioning. Please verify that chef
is installed and that the binary is available on the PATH.
```

you need to log into the vm and install chef-client:

```
vagrant ssh lp
sudo apt-get update
sudo apt-get install -y curl
curl -L https://www.opscode.com/chef/install.sh | sudo bash
exit
```

Back on the host machine, provision the VM

```
vagrant provision
```

You will need to add an entry to your host file to point to the installation

```
192.168.80.15   logicpull.local
```

Loginto the VM and run `sudo NODE_ENV=development node LogicPull.js`

Now you can go to `logicpull.local:3000` in the browser and you should see the login page.