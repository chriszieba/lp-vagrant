package "make" do
  action :install
end

package "build-essential" do
  action :install
end

package "libssl-dev" do
  action :install
end

package "zip" do
  action :install
end

package "g++" do
  action :install
end

package "git" do
  action :install
end

package "graphviz" do
  action :install
end

package "pdftk" do
  action :install
end

package "openjdk-7-jdk" do
  action :install
end

package "mongodb-server" do
  action :install
end

log "Installing Nodejs"
bash "nodejs" do
  user "root"
  cwd "/opt"
  code <<-EOH
    wget http://nodejs.org/dist/v0.8.8/node-v0.8.8.tar.gz
    tar -zxf node-v0.8.8.tar.gz
    cd node-v0.8.8
    ./configure && make && sudo make install
    EOH
end

log "Installing Forever"
bash "npm" do
  user "root"
  cwd "/tmp"
  code <<-EOH
    sudo npm install npm -g --ca=""
    sudo npm install forever -g
    EOH
end

log "Installing Apache FOP"
bash "fop" do
  user "root"
  cwd "/opt"
  code <<-EOH
    wget http://apache.mirror.nexicom.net/xmlgraphics/fop/binaries/fop-1.1-bin.zip
    unzip fop-1.1-bin.zip -d /opt/
    sudo chmod a+x /opt/fop-1.1/fop
    EOH
end

log "Installing LogicPull"
bash "logicpull" do
  user "root"
  cwd "/srv"
  code <<-EOH
    sudo mkdir /srv/www
    cd /srv/www
    git clone https://github.com/ChrisZieba/LogicPull.git

    cd /srv/www/LogicPull
    sudo mkdir generated uploads logs generated/output generated/answers generated/tmp uploads/deliverables logs/forever public/javascripts/preload
    sudo chmod -R 755 public/
    mongo LogicPull /srv/www/LogicPull/bin/db/init.js

    cd /srv/www/LogicPull
    git update-index --assume-unchanged public/robots.txt
    git update-index --assume-unchanged public/sitemap.xml
    git update-index --assume-unchanged config.js
    git update-index --assume-unchanged Gruntfile.js
    cd node_modules/
    git update-index --assume-unchanged $(git ls-files | tr '\n' ' ')

    cd /srv/www/LogicPull
    rm -rf node_modules/
    sudo npm install
    EOH
end

# Install a config file
  template "config.js" do
    path "/srv/www/LogicPull/config.js"
    source "config.js.erb"
    owner "root"
    group "root"
    mode "0755"
  end